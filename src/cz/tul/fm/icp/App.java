package cz.tul.fm.icp;

import cz.tul.fm.icp.objects.Lighting;
import cz.tul.fm.icp.camera.Motion;
import cz.tul.fm.icp.camera.Bullet;
import cz.tul.fm.icp.camera.Camera;
import cz.tul.fm.icp.camera.Player;
import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;
import static org.lwjgl.util.glu.GLU.gluPerspective;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.opengl.GLContext;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.util.ResourceLoader;

import cz.tul.fm.icp.objects.Ammo;
import cz.tul.fm.icp.objects.CrossHair;
import cz.tul.fm.icp.objects.Skybox;
import cz.tul.fm.icp.objects.Sound;
import cz.tul.fm.icp.objects.Spaceship;
import cz.tul.fm.icp.objects.State;
import cz.tul.fm.icp.particleEffects.ParticleEffect;
import cz.tul.fm.icp.particleEffects.Point;
import cz.tul.fm.icp.utils.RandomService;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.lwjgl.glfw.GLFWMouseButtonCallback;


public class App {

	private static final int DEFAULT_WIDTH = 800;
	private static final int DEFAULT_HEIGH = 600;

	private GLFWErrorCallback errorCallback;
	private GLFWKeyCallback keyCallback;
	private GLFWCursorPosCallback posCallback;
	private GLFWMouseButtonCallback mouseCallback;

	private long window;
	private int cenX, cenY;
	private final boolean isFullscreen = true;

	private RandomService randomSer;

	private Skybox skybox;

	private Field field;
	private Lighting lighting;
	private State state;

	private Camera camera;
	private Player player;
	private Bullet bullet;
	private Motion motion;
	private CrossHair crossHair;
	
	private int falling = 0;
	
	// nepřítel
	Spaceship ship;
	private boolean shipEnabled = true;

	// náboje
	List<Ammo> ammos = new CopyOnWriteArrayList<>();

	double lastTime;
	double fpsTime = 0;
	int frames = 0;

	private UnicodeFont font;
	private int actualWidth;
	private int actualHeight;
	private double actualFps = 0;
	private ParticleEffect particleEffect;
	private Double explosionDelay;
	private Point destroyPosition;
	
	private Sound sound;

	public void run() {
		try {
			init();
			loop();
			glfwDestroyWindow(window);
			keyCallback.release();
			posCallback.release();
			mouseCallback.release();
		} finally {
			glfwTerminate();
			errorCallback.release();
		}
	}

	private void init() {
		glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));

		if (glfwInit() != GL_TRUE) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}

		// konfigurace okna
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); // the window will be resizable

		createWindow(isFullscreen);

		lastTime = glfwGetTime();

		randomSer = new RandomService();

		// generování herní plochy
		field = new Field(500, 500, 100, randomSer);
		skybox = new Skybox();
                
		state = State.DAY;
		// osvetleni
		lighting = new Lighting();

		camera = player = new Player(randomSer, field);
		bullet = new Bullet();
		motion = new Motion();
		crossHair = new CrossHair(actualWidth, actualHeight);

		ship = new Spaceship(randomSer, field);
		
		sound = new Sound();
		
		sound.playBirds();
		
		initFonts();
	}

	private void generateAmmo() {
		for (int i = 0; i < randomSer.get(30, 50); i++) {
			int x = randomSer.get(0, field.getLength()-1);
			int y = randomSer.get(0, field.getWidth()-1);
			ammos.add(new Ammo(x, y, field.getHeight(x, y) + 0.5f));
		}
	}

	@SuppressWarnings("unchecked")
	private void initFonts() {
		try {
			InputStream inputStream = ResourceLoader.getResourceAsStream("res/fonts/HighlandGothicFLF.ttf");

			Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			awtFont = awtFont.deriveFont(38f); // set font size
			font = new UnicodeFont(awtFont);
			font.getEffects().add(new ColorEffect(java.awt.Color.white));
			font.addAsciiGlyphs();
			font.loadGlyphs();
		} catch (FontFormatException | IOException | SlickException e) {
			throw new RuntimeException("font loading error", e);
		}
	}

	private void createWindow(boolean fullscreen) {
		long win;
		long monitor = glfwGetPrimaryMonitor();
		ByteBuffer vidmode = glfwGetVideoMode(monitor);
		int screenWidth, screenHeight;
		screenWidth = GLFWvidmode.width(vidmode);
		screenHeight = GLFWvidmode.height(vidmode);


		actualWidth = screenWidth;
		actualHeight = screenHeight;
		if (!fullscreen) {
			monitor = NULL;
			actualWidth = DEFAULT_WIDTH;
			actualHeight = DEFAULT_HEIGH;
		}

		cenX = actualWidth/2;
		cenY = actualHeight/2;

		win = glfwCreateWindow(actualWidth, actualHeight, "D ICP App", monitor, NULL);
		if (win == NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}
		glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		if (!fullscreen) {
			glfwSetWindowPos(win, (screenWidth - actualWidth) / 2, (screenHeight - actualHeight) / 2);
		}

		glfwSetKeyCallback(win, keyCallback = new GLFWKeyCallback() {
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (action == GLFW_RELEASE) {
					switch (key) {
					case GLFW_KEY_ESCAPE:
					case GLFW_KEY_Q:
						Sound.killThreads();
						glfwSetWindowShouldClose(window, GL_TRUE);
						break;
					case GLFW_KEY_R:
						player.respawn();
						break;
					case GLFW_KEY_X:
						shoot();
						break;
					case GLFW_KEY_I:
						player.switchImmortality();
						break;
					case GLFW_KEY_U:
						shipEnabled = !shipEnabled;
						break;
					case GLFW_KEY_L:
						state = state.getNext();
						skybox.setState(state);
						lighting.setState(state);
						break;
					case GLFW_KEY_1:
						player.takeAmmo(5);
						break;
					}
				}
			}
		});
		glfwSetCursorPosCallback(win, posCallback = new GLFWCursorPosCallback() {

			@Override
			public void invoke(long window, double x, double y) {
				camera.rotateY((float) (cenX - x) / cenX*20);
				camera.rotateUpDown((float) (y - cenY) / cenY*20);
			}
		});
		glfwSetMouseButtonCallback(win, mouseCallback = new GLFWMouseButtonCallback() {

			@Override
			public void invoke(long window, int button, int action, int mods) {
				if (action == GLFW_PRESS) {
					if (button == GLFW_MOUSE_BUTTON_1) {
						shoot();
					}
				}
			}
		});


		// Make the OpenGL context current
		glfwMakeContextCurrent(win);
		// Enable v-sync
		glfwSwapInterval(1);

		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the ContextCapabilities instance and makes the OpenGL
		// bindings available for use.
		GLContext.createFromCurrent();

		initGl();
		reshape(actualWidth, actualHeight);

		glfwShowWindow(win);
		if (this.window != NULL) {
			glfwDestroyWindow(this.window);
		}
		window = win;
	}

	private void shoot() {
		if (camera == player && player.getAmmo() > 0) {
			player.useAmmo();
			camera = bullet;
			bullet.setPosition(player);
			sound.playShoot();
		}
	}

	private void resolveMotion(double delta) {
		motion.reset();

		if (isPressed(GLFW_KEY_LEFT_SHIFT)) {
			motion.speed = 3f;
		}
		
		if (isPressed(GLFW_KEY_LEFT_CONTROL)) {
			motion.speed = 0f;
		}
		motion.forward = isPressed(GLFW_KEY_W);
		motion.back = isPressed(GLFW_KEY_S);
		motion.left = isPressed(GLFW_KEY_A);
		motion.right = isPressed(GLFW_KEY_D);
		motion.jump = isPressed(GLFW_KEY_SPACE);

		boolean isWalking = motion.forward | motion.back | motion.left | motion.right;
		Sound.setIsWalking(isWalking);
		
		camera.move(motion, delta);

		// natočení lodi
		if (shipEnabled) {
			ship.directTo(player.getPosX(), player.getPosZ());
			ship.update(delta);
		}
		
		// reflektor pod lodi
		if( lighting.isNight() ){
			float tempY = field.getHeight(ship.getXpos(), ship.getZpos());
			if( tempY > -Float.MAX_VALUE ){
				lighting.setSpotLightPosition( ship.getXpos(), tempY + 20, ship.getZpos() );
				lighting.render();
			}
		}

		// kolize kulky
		if (camera == bullet) {
			float x = bullet.getPosX(), z = bullet.getPosZ(), y = bullet.getPosY();
			if (ship.isCollision(x, z, y) || y - field.getHeight(x, z) < 2) {
				explosion(x, z, y);
				sound.playBlik();
			}

		// kolize herni postavy s nabojema
		} else {
			
			float x = player.getPosX(), z = player.getPosZ(), y = player.getPosY();
			
			if(field.outOfField(x, z)){
				falling ++;
				if(falling > 20 && !sound.isHajzliPlayed()){
					sound.playHajzli();
					Sound.setHajzliPlayed(true);
				}
			}else{
				falling = 0;
			}
			
			for (Ammo ammo : ammos) {
				if (ammo.isCollision(x, z, y)) {
					player.takeAmmo(randomSer.get(1, 3));
					ammos.remove(ammo);
					sound.playLoad();
				}
			}
		}

		// střílení lodi
		if (shipEnabled) {
			float x = player.getPosX(), z = player.getPosZ();
			if (ship.isNear(x, z)) {
				sound.playPlayerHit();
				explosion(x, z, player.getPosY());
			}
		}

		// další ammo
		if (ammos.size() <= 10) {
			generateAmmo();
		}
	}

	private void explosion(float x, float z, float y) {
		for (Ammo ammo : ammos) {
			if (ammo.isDestroyedFrom(x, z, y, Bullet.POWER)) {
				ammos.remove(ammo);
			}
		}
		if (ship.isDestroyedFrom(x, z, y, Bullet.POWER)) {
			ship.respawn();
			player.hit();
			destroyPosition = new Point(x, y, z);
			camera.setActive(false);
			particleEffect = new ParticleEffect(2000, new Point(0, 0, -15f));
			explosionDelay = 1.0;
			return;
		}
		field.destroyFrom(x, z, y, Bullet.POWER);
		player.destroyFrom(x, z, y, Bullet.POWER);
		camera = player;
	}

	private boolean isPressed(int k) {
		return glfwGetKey(window, k) == GLFW_PRESS;
	}

	private void initGl() {
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE);
		glClearDepth(1.0f);
		glDepthFunc(GL_LESS);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);
		glLineWidth(2.0f);
		glShadeModel(GL_SMOOTH);
		glClearStencil(0);                                  // Stencil Buffer Setup
	        glDepthFunc(GL_LEQUAL);                             // The Type Of Depth Testing To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Really Nice Perspective Calculations
	}

	private void reshape(int width, int height) {
		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(45.0f, (float) width/height, 0.1f, 30000.0f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

	private void loop() {
		while (glfwWindowShouldClose(window) == GL_FALSE) {

			double now = glfwGetTime();
			double delta = now - lastTime;
			lastTime = now;

			// bullet time
			if (isPressed(GLFW_KEY_LEFT_ALT)) {
				delta /= 2.5;
			}

			fpsTime += delta;
			if (fpsTime >= 1) {
				actualFps = frames/fpsTime;
				fpsTime = 0;
				frames = 0;
			}
			frames++;

			glfwSetCursorPos(window, cenX, cenY); // vystředit pozici kurzoru
			resolveMotion(delta);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			camera.curlView();

			// skybox vykreslit ještě před posunutím scény -> posouvá se s hráčem.
			skybox.render();

			camera.moveToPosition();
			
			// vykreslení osvětlení
			lighting.render();

			renderCoords();
			
			field.render();

			for(Ammo ammo : ammos) {
				ammo.render();
			}

			ship.render();
			
			if (explosionDelay != null) {
				explosionDelay -= delta;
				
				if (explosionDelay <= 0) {
					field.destroyFrom(destroyPosition.getX(), destroyPosition.getZ(), destroyPosition.getY(), Bullet.POWER);
					player.destroyFrom(destroyPosition.getX(), destroyPosition.getZ(), destroyPosition.getY(), Bullet.POWER);
					
					camera.setActive(true);
					camera = player;
					explosionDelay = null;
				}
			}

			if (camera == bullet) {
				player.render();
			}

			if (particleEffect != null) {
				glLoadIdentity();
				particleEffect.update(delta);
				particleEffect.render();
			}

			glBindTexture(GL_TEXTURE_2D, 0); // unbindovat texturu
			enableOrthoView();

			if (player.getAmmo() > 0 || camera == bullet) {
				crossHair.setIsNight(state == State.NIGHT);
				crossHair.setBig(camera == bullet);
				crossHair.render();
			}

			font.drawString(20, 20, String.format("%.0f FPS", actualFps));
			font.drawString(actualWidth-150, actualHeight-80, String.format("%d %%", player.getHealth()));
			font.drawString(20, actualHeight-80, String.format("ammo: %d", player.getAmmo()));
			font.drawString(300, actualHeight-80, String.format("score: %d / %d", player.getHits(), player.getDeaths()));

			disableOrthoView();

			glfwSwapBuffers(window);
			glfwPollEvents();
			
			
		}
	}

	private void renderCoords() {
		glLineWidth(4.0f);
		glBegin(GL_LINES);
		glColor3f(1, 0, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(10, 0, 0);

		glColor3f(0, 1, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 10, 0);

		glColor3f(0, 0, 1);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 0, 10);
		glEnd();
	}

	private void enableOrthoView() {
		glDisable(GL_DEPTH_TEST);
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0, actualWidth, actualHeight, 0, -1, 1);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();
		// kvůli průhlednosti pozadí písma - jinak se zobrazuje černé
		glEnable(GL_BLEND);
	}

	private void disableOrthoView() {
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}
}
