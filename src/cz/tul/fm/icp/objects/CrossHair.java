package cz.tul.fm.icp.objects;

import static org.lwjgl.opengl.GL11.*;

public class CrossHair implements Renderable{
	
	private final int dispListId, smallDispListId;
	
	private final int crossStep = 60;
	
	private final float crossWidthInit = 70;
	
	private final float crossWidthStepInit = 5;
	
	private final int startPoint = 100;
	
	private final int actualWidth;
	
	private final int actualHeight;
	
	private boolean isNight = false;

	private boolean big = false;

	public void setIsNight(boolean isNight) {
		this.isNight = isNight;
	}


	public CrossHair(int windowWidth, int windowHeight) {
		
		actualWidth = windowWidth;
		actualHeight = windowHeight;

		dispListId = glGenLists(1);
		smallDispListId = glGenLists(1);
		createDispLists();
	}

	public void setBig(boolean big) {
		this.big = big;
	}

	@Override
	public void render() {
		if( isNight ){
			glColor4f(1f, 1f, 1f, 1f);
		}else{
			glColor4f(0f, 0f, 0f, 1f);
		}
		
		boolean materialEnabled = glIsEnabled(GL_COLOR_MATERIAL);
		
		if( !materialEnabled ){
			glEnable(GL_COLOR_MATERIAL);
		}
		
		glDisable(GL_BLEND);
		glPushMatrix();
		if (big) {
			glLineWidth(1.0f);
			glCallList(dispListId);
		}
		glLineWidth(3.0f);
		glCallList(smallDispListId);
		glPopMatrix();
		glEnable(GL_BLEND);
		
		if( !materialEnabled ){
			glDisable(GL_COLOR_MATERIAL);
		}
	}
	
	private void createDispLists() {
		glNewList(dispListId, GL_COMPILE);
		
			glBegin(GL_LINES);

				glVertex2f( 0, (actualHeight / 2) + 1 );
				glVertex2f( actualWidth, (actualHeight / 2) + 1 );
				glVertex2f( (actualWidth / 2) + 1, 0 );
				glVertex2f( (actualWidth / 2) + 1, actualHeight );
				
				int tmpStep = ( actualHeight / 2 ) + startPoint;
				float w = crossWidthInit;
				float ws = crossWidthStepInit;
				
				while( tmpStep < actualHeight && w > 0 ){
					glVertex2f( ( actualWidth / 2 ) - ( w / 2 ), tmpStep );
					glVertex2f( ( actualWidth / 2 ) + ( w / 2 ), tmpStep );
					w = w - ws;
					tmpStep = tmpStep + crossStep;
				}
				
				tmpStep = ( actualWidth / 2 ) - crossStep;
				w = crossWidthInit;
				ws = crossWidthStepInit;
				
				while( tmpStep > 0 && w > 0 ){
					glVertex2f( tmpStep, ( actualHeight / 2 ) - ( w / 2 ) );
					glVertex2f( tmpStep, ( actualHeight / 2 ) + ( w / 2 ) );
					w = w - ws;
					tmpStep = tmpStep - crossStep;
				}
				
				tmpStep = ( actualWidth / 2 ) + crossStep;
				w = crossWidthInit;
				ws = crossWidthStepInit;
				
				while( tmpStep < actualWidth && w > 0 ){
					glVertex2f( tmpStep, ( actualHeight / 2 ) - ( w / 2 ) );
					glVertex2f( tmpStep, ( actualHeight / 2 ) + ( w / 2 ) );
					w = w - ws;
					tmpStep = tmpStep + crossStep;
				}
				
				
			glEnd();
		glEndList();

		glNewList(smallDispListId, GL_COMPILE);
		glBegin(GL_LINES);

		int width = 30;
		int cenX = actualWidth/2 + 1,
			cenY = actualHeight/2 + 1;

		glVertex2f(cenX-width, cenY);
		glVertex2f(cenX+width, cenY);
		glVertex2f(cenX, cenY-width);
		glVertex2f(cenX, cenY+width);

		glEnd();
		glEndList();
	}
	
}
