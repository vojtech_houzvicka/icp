package cz.tul.fm.icp.objects;

public enum State {
	DAY,
	NIGHT;
	public State getNext() {
		return values()[(ordinal()+1) % values().length];
	}
}
