package cz.tul.fm.icp.objects;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;
import static org.lwjgl.opengl.GL11.*;


public class Skybox implements Renderable {

	private final Map<State, Texture> textures;
	private int dispListId;
	private static final float skyBoxSize = 10000.0f;
	private State state = State.DAY;
	
	public Skybox() {
		this.textures = new HashMap<>();
		loadTextures();
		createDispList();
	}

	private void loadTextures(){
		try {
			textures.put(State.DAY, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/tex/skybox_day.png"), GL_LINEAR));
			textures.put(State.NIGHT, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/tex/skybox_night.png"), GL_LINEAR));
		} catch (IOException e) {
			throw new RuntimeException("skybox loading error");
		}
	}

	private void createDispList() {
		dispListId = glGenLists(1);

		glNewList(dispListId, GL_COMPILE);

		glBegin(GL_QUADS);

		// předek
		glTexCoord2f(1/4f, 2/4f);
		glVertex3f(-skyBoxSize, -skyBoxSize, -skyBoxSize);
		glTexCoord2f(1/4f, 1/4f);
		glVertex3f(-skyBoxSize, skyBoxSize, -skyBoxSize);
		glTexCoord2f(2/4f, 1/4f);
		glVertex3f(skyBoxSize, skyBoxSize, -skyBoxSize);
		glTexCoord2f(2/4f, 2/4f);
		glVertex3f(skyBoxSize, -skyBoxSize, -skyBoxSize);

		// napravo
		glTexCoord2f(2/4f, 2/4f);
		glVertex3f(skyBoxSize, -skyBoxSize, -skyBoxSize);
		glTexCoord2f(2/4f, 1/4f);
		glVertex3f(skyBoxSize, skyBoxSize, -skyBoxSize);
		glTexCoord2f(3/4f, 1/4f);
		glVertex3f(skyBoxSize, skyBoxSize, skyBoxSize);
		glTexCoord2f(3/4f, 2/4f);
		glVertex3f(skyBoxSize, -skyBoxSize, skyBoxSize);

		// zadek
		glTexCoord2f(3/4f, 2/4f);
		glVertex3f(skyBoxSize, -skyBoxSize, skyBoxSize);
		glTexCoord2f(3/4f, 1/4f);
		glVertex3f(skyBoxSize, skyBoxSize, skyBoxSize);
		glTexCoord2f(4/4f, 1/4f);
		glVertex3f(-skyBoxSize, skyBoxSize, skyBoxSize);
		glTexCoord2f(4/4f, 2/4f);
		glVertex3f(-skyBoxSize, -skyBoxSize, skyBoxSize);

		// nalevo
		glTexCoord2f(0/4f, 2/4f);
		glVertex3f(-skyBoxSize, -skyBoxSize, skyBoxSize);
		glTexCoord2f(0/4f, 1/4f);
		glVertex3f(-skyBoxSize, skyBoxSize, skyBoxSize);
		glTexCoord2f(1/4f, 1/4f);
		glVertex3f(-skyBoxSize, skyBoxSize, -skyBoxSize);
		glTexCoord2f(1/4f, 2/4f);
		glVertex3f(-skyBoxSize, -skyBoxSize, -skyBoxSize);

		// podlaha
		glTexCoord2f(1/4f, 3/4f);
		glVertex3f(-skyBoxSize, -skyBoxSize, skyBoxSize);
		glTexCoord2f(1/4f, 2/4f);
		glVertex3f(-skyBoxSize, -skyBoxSize, -skyBoxSize);
		glTexCoord2f(2/4f, 2/4f);
		glVertex3f(skyBoxSize, -skyBoxSize, -skyBoxSize);
		glTexCoord2f(2/4f, 3/4f);
		glVertex3f(skyBoxSize, -skyBoxSize, skyBoxSize);

		// strop
		glTexCoord2f(1/4f, 1/4f);
		glVertex3f(-skyBoxSize, skyBoxSize, -skyBoxSize);
		glTexCoord2f(1/4f, 0/4f);
		glVertex3f(-skyBoxSize, skyBoxSize, skyBoxSize);
		glTexCoord2f(2/4f, 0/4f);
		glVertex3f(skyBoxSize, skyBoxSize, skyBoxSize);
		glTexCoord2f(2/4f, 1/4f);
		glVertex3f(skyBoxSize, skyBoxSize, -skyBoxSize);
		glEnd();

		glEndList();
	}

	public void cleanup() {
		textures.get(State.DAY).release();
		textures.get(State.NIGHT).release();
	}

	@Override
	public void render() {
		glColor3f(1, 1, 1);
		textures.get(state).bind();
		glPushMatrix();
		glCallList(dispListId);
		glPopMatrix();
	}
	
	public void setState(State state) {
		this.state = state;
	}
}
