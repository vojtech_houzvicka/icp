/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.tul.fm.icp.objects;

import org.lwjgl.BufferUtils;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Administrator
 */
public class Lighting implements Renderable{
	
	// nehe
	float matAmb[] = {0.4f, 0.4f, 0.4f, 1.0f}; // Material - Ambient Values
	float matDif[] = {0.2f, 0.6f, 0.9f, 1.0f}; // Material - Diffuse Values
	float matSpc[] = {0.0f, 0.0f, 0.0f, 1.0f}; // Material - Specular Values
	float matShn[] = {0.0f, 0.0f, 0.0f, 0.0f}; // Material - Shininess
	
	ByteBuffer byteBuffer;
	ByteBuffer floatBuffer;
	
	private State state = State.DAY;
	
	private final FloatBuffer whiteSpecular;
	private final FloatBuffer whiteSpecularMaterial;
	private final FloatBuffer lightPosition;
	private final FloatBuffer spotLightPosition;
	private final FloatBuffer lightDirection;
	private final FloatBuffer whiteDiffuse;
	private final FloatBuffer whiteAmbient;
	
	public Lighting() {
		floatBuffer = ByteBuffer.allocateDirect(64);
		byteBuffer = ByteBuffer.allocateDirect(16);
		floatBuffer.order(ByteOrder.nativeOrder());
		byteBuffer.order(ByteOrder.nativeOrder());
		whiteSpecular = BufferUtils.createFloatBuffer(4);
		whiteSpecularMaterial = BufferUtils.createFloatBuffer(4);
		lightPosition = BufferUtils.createFloatBuffer(4);
		lightDirection = BufferUtils.createFloatBuffer(4);
		whiteDiffuse = BufferUtils.createFloatBuffer(4);
		whiteAmbient = BufferUtils.createFloatBuffer(4);
		spotLightPosition = BufferUtils.createFloatBuffer(4);
		spotLightPosition.put(0f).put(0f).put(0f).put(1.0f).flip();
	}
	
	public void setSpotLightPosition( float x, float y, float z ){
		spotLightPosition.put(x).put(y).put(z).put(1.0f).flip();
	}
	
	public boolean isNight( ){
		return state == State.NIGHT;
	}
	
	private void dayLight( ){
		initSunLightArrays( );

		glClearDepth(1);
		
		glEnable(GL_LIGHTING);
		
		glEnable(GL_COLOR_MATERIAL);
		
		glLight(GL_LIGHT0, GL_POSITION, lightPosition);
		
		glLight(GL_LIGHT0, GL_SPECULAR, whiteSpecular);
		glLight(GL_LIGHT0, GL_AMBIENT, whiteAmbient);
		glLight(GL_LIGHT0, GL_DIFFUSE, whiteDiffuse);
		
		glEnable(GL_LIGHT0);
	}
	
	private void nightLight(){
		glClearDepth(1);
		
		// ****** LIGHT0 settings ******
		initMoonLightArrays();
		
		glLight(GL_LIGHT0, GL_POSITION, lightPosition);
		
		glLight(GL_LIGHT0, GL_SPECULAR, whiteSpecular);
		glLight(GL_LIGHT0, GL_AMBIENT, whiteAmbient);
		glLight(GL_LIGHT0, GL_DIFFUSE, whiteDiffuse);
		// ****** end LIGHT0 settings ******
		
		glEnable(GL_LIGHT0);
		glEnable(GL_LIGHTING);
	}
	
	private void spotLight(){
		// ****** LIGHT1 settings ******
		initSpotLightArrays();
		
		// svetlo se vzddalenosti ztraci na intenzite
		glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, .5f);
		glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, .01f);
		
		glLight(GL_LIGHT1, GL_POSITION, spotLightPosition);
		
		glLight(GL_LIGHT1, GL_SPECULAR, whiteSpecular);
		glLight(GL_LIGHT1, GL_AMBIENT, whiteAmbient);
		glLight(GL_LIGHT1, GL_DIFFUSE, whiteDiffuse);
		
		glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 30.0f);
		glLight(GL_LIGHT1, GL_SPOT_DIRECTION, lightDirection);
		glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 2.0f);
		// ****** end LIGHT1 settings ******
		
		glEnable(GL_LIGHT1);
		glEnable(GL_LIGHTING);
	}
	
	private void disableAllLights(){
		glDisable(GL_COLOR_MATERIAL);
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHT1);
		glDisable(GL_LIGHTING);
	}
	
	public void setState(State state){
		this.state = state;
	}
	
	private void initSunLightArrays() {
		whiteSpecular.put(.1f).put(.5f).put(.5f).put(1f).flip();
		
		whiteSpecularMaterial.put(.3f).put(.3f).put(.3f).put(1f).flip();
		
		lightPosition.put(-9900f).put(4000f).put(-6000f).put(1.0f).flip();
		
		whiteDiffuse.put(.3f).put(.3f).put(.6f).put(1f).flip();
		
		whiteAmbient.put(0.6f).put(0.5f).put(0.6f).put(1f).flip();
	}
	
	private void initMoonLightArrays() {
		lightPosition.put(3450f).put(4250f).put(-9999f).put(1.0f).flip();
		
		whiteSpecular.put(.0f).put(.0f).put(.0f).put(1.0f).flip();
		
		whiteDiffuse.put(.1f).put(.1f).put(.2f).put(1.0f).flip();
		
		whiteAmbient.put(0.0f).put(0.0f).put(0.0f).put(1.0f).flip();
	}
	
	private void initSpotLightArrays() {
		lightDirection.put(0f).put(-1f).put(0f).put(1.0f).flip();
		
		whiteSpecular.put(.0f).put(.0f).put(.0f).put(1.0f).flip();
		
		whiteDiffuse.put(1f).put(1f).put(1f).put(1.0f).flip();
		
		whiteAmbient.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
	}
	
	@Override
	public void render() {
		switch( state ){ // z stejt
			case DAY:
				disableAllLights();
				dayLight();
				break;
			case NIGHT:
				disableAllLights();
				nightLight();
				spotLight();
				break;
		}
	}
	
}

/**
 * Ambient illumination is light that's been scattered so much by the environment 
 * that its direction is impossible to determine - it seems to come from all directions. 
 * Backlighting in a room has a large ambient component, since most of the light that 
 * reaches your eye has first bounced off many surfaces. A spotlight outdoors has a 
 * tiny ambient component; most of the light travels in the same direction, and since 
 * you're outdoors, very little of the light reaches your eye after bouncing off other 
 * objects. When ambient light strikes a surface, it's scattered equally in all directions.
 *
 * The diffuse component is the light that comes from one direction, so it's brighter if 
 * it comes squarely down on a surface than if it barely glances off the surface. Once it 
 * hits a surface, however, it's scattered equally in all directions, so it appears equally 
 * bright, no matter where the eye is located. Any light coming from a particular position 
 * or direction probably has a diffuse component.
 * 
 * Specular light comes from a particular direction, and it tends to bounce off the surface 
 * in a preferred direction. A well-collimated laser beam bouncing off a high-quality mirror 
 * produces almost 100 percent specular reflection. Shiny metal or plastic has a high specular 
 * component, and chalk or carpet has almost none. You can think of specularity as shininess.
 */
