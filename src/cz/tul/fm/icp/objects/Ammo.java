package cz.tul.fm.icp.objects;

import cz.tul.fm.icp.objLoader.Model;
import cz.tul.fm.icp.objLoader.ObjLoader;
import java.io.File;
import java.io.IOException;
import org.newdawn.slick.opengl.Texture;
import static org.lwjgl.opengl.GL11.*;

public class Ammo {

	private static Model model;
	private static int objDL;

	private float xpos, zpos, ypos;

	private static void loadModel(String path) {
		if (model == null) {
			try {
				model = ObjLoader.loadTexturedModel(new File(path));
				objDL = ObjLoader.createTexturedDisplayList(model);
			} catch (IOException e) {
				throw new RuntimeException("object loading error", e);
			}
		}
	}

	public Ammo(float xpos, float zpos, float ypos) {
		this.xpos = xpos;
		this.zpos = zpos;
		this.ypos = ypos;

		loadModel("res/models/Cage_Container/Cage_Container.obj");
	}

	public void render() {
		for (Texture texture : model.getTextures()) {
			texture.bind();
		}
		glPushMatrix();
		glTranslatef(xpos, ypos, zpos);
		glScaled(.25, .25, .25);
		glCallList(objDL);
		glPopMatrix();
	}

	public boolean isCollision(float x, float z, float y) {
		float xr = x-xpos, zr = z-zpos, yr = y-ypos;
		return Math.sqrt(xr*xr + zr*zr + yr*yr) < 4f;
	}

	public boolean isDestroyedFrom(float x, float z, float y, int radius) {
		float xr = x-xpos, zr = z-zpos, yr = y-ypos;
		return Math.sqrt(xr*xr + zr*zr + yr*yr) < radius;
	}
}
