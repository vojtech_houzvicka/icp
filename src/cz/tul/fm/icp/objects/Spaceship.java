package cz.tul.fm.icp.objects;

import cz.tul.fm.icp.Field;
import cz.tul.fm.icp.objLoader.Model;
import cz.tul.fm.icp.objLoader.ObjLoader;
import cz.tul.fm.icp.utils.GMath;
import cz.tul.fm.icp.utils.RandomService;
import java.io.File;
import java.io.IOException;
import org.newdawn.slick.opengl.Texture;
import static org.lwjgl.opengl.GL11.*;

public class Spaceship {
	private static final float ABOVE_FLOOR = 17;
	private static final float SPEED = 30f;
	private static final String MODEL_PATH = "res/models/Quad 45fg transport/Quad 45fg transport.obj";

	private final RandomService randomSer;

	private Model model;
	private int objDL;

	private Field floor;

	private float xpos, zpos, ypos;
	private double goalAngle, yrot = 0;

	public Spaceship(RandomService randomSer, Field floor) {
		try {
			model = ObjLoader.loadTexturedModel(new File(MODEL_PATH));
			objDL = ObjLoader.createTexturedDisplayList(model);
		} catch (IOException e) {
			throw new RuntimeException("object loading error", e);
		}

		this.randomSer = randomSer;
		this.floor = floor;
		respawn();
	}

	public float getXpos() {
		return xpos;
	}

	public float getZpos() {
		return zpos;
	}

	public float getYpos() {
		return ypos;
	}

	public void respawn() {
		xpos = randomSer.get(0, floor.getLength()-1);
		zpos = randomSer.get(0, floor.getWidth()-1);
	}

	public void render() {
		for (Texture texture : model.getTextures()) {
			texture.bind();
		}
		glPushMatrix();
		ypos =  floor.getHeight(sanitize(xpos, 0, floor.getLength()-2),
			sanitize(zpos, 0, floor.getWidth()-2)) + ABOVE_FLOOR;
		glTranslated(xpos, ypos, zpos);
		glRotated(yrot, 0, 1, 0);
		glScaled(0.025, 0.025, 0.025);
		glCallList(objDL);
		glPopMatrix();
	}

	private float sanitize(float val, float min, float max) {
		if (val < 0) {
			return min;
		} else if (val > max) {
			return max;
		}
		return val;
	}

	public void directTo(float x, float z) {
		float xrel = x - xpos;
		float zrel = z - zpos;

		goalAngle = GMath.atan2(xrel, zrel);
	}

	public void update(double deltaTime) {
		double diff = goalAngle - yrot,
			diff2 = diff + (goalAngle > yrot ? -360 : 360);
		if (Math.abs(diff2) < Math.abs(diff)) {
			diff = diff2;
		}
		if (Math.abs(diff) < 1) {
			yrot = goalAngle;
		} else {
			double d = 30 * deltaTime;
			yrot += d * (diff > 0 ? 1 : -1);
		}

		xpos += GMath.sin(yrot) * SPEED*deltaTime;
		zpos += GMath.cos(yrot) * SPEED*deltaTime;
	}

	public boolean isCollision(float x, float z, float y) {
		float xr = x-xpos, zr = z-zpos, yr = y-ypos;
		return Math.sqrt(xr*xr + zr*zr + yr*yr) < 10f;
	}

	public boolean isNear(float x, float z) {
		float xr = x-xpos, zr = z-zpos;
		return Math.sqrt(xr*xr + zr*zr) < 10f;
	}

	public boolean isDestroyedFrom(float x, float z, float y, int radius) {
		float xr = x-xpos, zr = z-zpos, yr = y-ypos;
		return Math.sqrt(xr*xr + zr*zr + yr*yr) < radius;
	}
}
