/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tul.fm.icp.objects;

import cz.tul.fm.icp.utils.RandomService;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 *
 * @author Administrator
 */
public class Sound {

	public static boolean isWalking;
	
	public static boolean isRunning;

	private static File audioFile;

	private static final File audioFileWalk = new File("res/audio/walk.wav");
	
	private static final List<Thread> threads = new ArrayList<>();
	
	private static boolean appRunning = true;
	
	private static boolean hajzliPlayed = true;

	public Sound() {
		walk();
	}

	public static void setHajzliPlayed(boolean hajzliPlayed) {
		Sound.hajzliPlayed = hajzliPlayed;
	}

	public static boolean isHajzliPlayed() {
		return hajzliPlayed;
	}

	public static void setIsWalking(boolean isWalking) {
		Sound.isWalking = isWalking;
	}

	public static void setIsRunning(boolean isRunning) {
		Sound.isRunning = isRunning;
	}

	public static void setAppRunning(boolean appRunning) {
		Sound.appRunning = appRunning;
	}
	
	public void playShoot() {
		audioFile = new File("res/audio/shooting.wav");
		playSound();
	}

	public void playBlik() {
		audioFile = new File("res/audio/blik.wav");
		playSound();
	}

	public void playBirds() {
		audioFile = new File("res/audio/birds.wav");
		playSound();
	}
	
	public void playNomore() {
		audioFile = new File("res/audio/nomore.wav");
		playSound();
	}
	
	public void playLoad() {
		audioFile = new File("res/audio/load.wav");
		playSound();
	}

	public static boolean isAppRunning() {
		return appRunning;
	}
	
	public void playPlayerHit() {
		RandomService rs = new RandomService();
		int i = rs.get(1, 9);
		audioFile = new File("res/audio/playerHit (" + i +").wav");
		playSound();
	}
	
	public void playHajzli() {
		audioFile = new File("res/audio/playerHit (3).wav");
		playSound();
	}

	private static synchronized void playSound() {
		Thread t = new Thread(new Runnable() {
			// The wrapper thread is unnecessary, unless it blocks on the
			// Clip finishing; see comments.
			@Override
			public void run() {
				try {
					Clip clip = AudioSystem.getClip();
					AudioInputStream inputStream = AudioSystem.getAudioInputStream(audioFile);
					clip.open(inputStream);
					clip.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
		threads.add(t);
	}

	private static synchronized void walk() {
		Thread t = new Thread(new Runnable() {
			// The wrapper thread is unnecessary, unless it blocks on the
			// Clip finishing; see comments.
			@Override
			public void run() {
				Sound.setAppRunning(true);
				while (Sound.isAppRunning()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException ex) {
						Logger.getLogger(Sound.class.getName()).log(Level.SEVERE, null, ex);
					}
					if(!appRunning)
						break;
					if(isWalking) {
						try {
							Clip clip = AudioSystem.getClip();
							AudioInputStream inputStream = AudioSystem.getAudioInputStream(audioFileWalk);
							clip.open(inputStream);
							clip.start();
							while (clip.getMicrosecondLength() != clip.getMicrosecondPosition()) {
								if(!appRunning){
									clip.stop();
									break;
								}
								if(!isWalking){
									clip.stop();
									break;
								}
								
							}
						} catch (Exception e) {
							Logger.getLogger(Sound.class.getName()).log(Level.SEVERE, null, e);
						}
					}
				}
			}
		});
		t.start();
		threads.add(t);
	}
	
	public static void killThreads(){
		Sound.appRunning = false;
		for(Thread t: threads){
			t.interrupt();
		}
	}

}
