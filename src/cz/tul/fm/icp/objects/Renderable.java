package cz.tul.fm.icp.objects;

public interface Renderable {

	public void render();

}
