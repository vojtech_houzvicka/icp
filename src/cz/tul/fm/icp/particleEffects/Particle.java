package cz.tul.fm.icp.particleEffects;

public class Particle {
	
	protected boolean active;

	protected float life;
	protected float fade;

	protected float red;
	protected float green;
	protected float blue;

	protected float xPosition;
	protected float yPosition;
	protected float zPosition;

	protected float xDirection;
	protected float yDirection;
	protected float zDirection;

	protected float xGravity = 0;
	protected float yGravity = -0.15f;
	protected float zGravity = 0;

	public void makeOlder(float oldness) {
		this.life -= oldness;
		
		if (this.life <= 0) {
			this.life = 0;
			active = false;
		}
	}
}
