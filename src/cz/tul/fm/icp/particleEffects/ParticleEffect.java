package cz.tul.fm.icp.particleEffects;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import cz.tul.fm.icp.objects.Renderable;
import static org.lwjgl.opengl.GL11.*;

public class ParticleEffect implements Renderable {

	private final int maxParticles;

	/**
	 * Zpomalení částic
	 */
	private double deltaTime = 0f;

	private final float colors[][] = { { 1.0f, 0.5f, 0.5f }, { 1.0f, 0.75f, 0.5f }, { 1.0f, 1.0f, 0.5f }, { 0.75f, 1.0f, 0.5f }, { 0.5f, 1.0f, 0.5f }, { 0.5f, 1.0f, 0.75f }, { 0.5f, 1.0f, 1.0f }, { 0.5f, 0.75f, 1.0f }, { 0.5f, 0.5f, 1.0f }, { 0.75f, 0.5f, 1.0f }, { 1.0f, 0.5f, 1.0f }, { 1.0f, 0.5f, 0.75f } };

	private final Particle[] particles;

	private Texture texture;

	private static final String TEXTURE_IMAGE = "res/tex/particle.png";
	
	private final Point position;
	
	private boolean active;

	public ParticleEffect(int maxParticles, Point position) {
		this.maxParticles = maxParticles;
		this.position = position;
		particles = new Particle[maxParticles];
		init();
	}

	public void update(double deltaTime) {
		this.deltaTime = deltaTime;
	}

	@Override
	public void render() {
		if (!active) {
			return;
		}
		
		int numberOfInactive = 0;

		glEnable(GL_BLEND);
		
		for (int i = 0; i < maxParticles; i++) {
			Particle particle = particles[i];
			
			if (!particle.active) {
				numberOfInactive++;
				continue;
			}
			
			float x = particle.xPosition;
			float y = particle.yPosition;
			float z = particle.zPosition;
			
			// nastavení barvy částice - životnost se používá pro průhlednost -> částice postupně vybledne
			glColor4f(particle.red, particle.green, particle.blue, particle.life);
			texture.bind();

			// vytvoříme obdélník pomocí triangle stripu
			glBegin(GL_TRIANGLE_STRIP);
				// Horní pravý
				glTexCoord2d(1, 1); 
				glVertex3f(x + 0.05f, y + 0.05f, z);
				
				// Horní levý
				glTexCoord2d(0, 1);
				glVertex3f(x - 0.05f, y + 0.05f, z);
				
				// Dolní pravý
				glTexCoord2d(1, 0); 
				glVertex3f(x + 0.05f, y - 0.05f, z);
				
				// Dolní levý
				glTexCoord2d(0, 0);
				glVertex3f(x - 0.05f, y - 0.05f, z);
			glEnd();
			
			// nastavení pohybu částice
			double slowdown = deltaTime * 0.05;
			particle.xPosition += (float) (particle.xDirection * slowdown);
			particle.yPosition += (float) (particle.yDirection * slowdown);
			particle.zPosition += (float) (particle.zDirection * slowdown);

			// působení gravitace
			particle.xPosition += particle.xGravity * slowdown;
			particle.yPosition += particle.yGravity * slowdown;
			particle.zPosition += particle.zGravity * slowdown;

			// snížení životnosti o stárnutí
			particle.makeOlder(particle.fade);
		}
		
		glDisable(GL_BLEND);
		
		if (numberOfInactive == maxParticles) {
			active = false;
		}
	}
	
	public void reset() {
		for (int i = 0; i < maxParticles; i++) {
			initParticle(particles[i], i);
		}
		
		active = true;
	}

	private void init() {
		try {
			texture = TextureLoader.getTexture("png", new FileInputStream(new File(TEXTURE_IMAGE)));
		} catch (IOException e) {
			throw new RuntimeException("texture loading error", e);
		}

		glShadeModel(GL_SMOOTH);// Povolíme jemné stínování
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);// Černé pozadí
		glClearDepth(1.0f);// Nastavení hloubkového bufferu
		glDisable(GL_DEPTH_TEST);// Vypne hloubkové testování
		glEnable(GL_BLEND);// Zapne blending
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);// Typ blendingu
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);// Perspektiva
		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);// Jemnost bodů
		glEnable(GL_TEXTURE_2D);// Zapne mapování textur
		glBindTexture(GL_TEXTURE_2D, texture.getTextureID());// Vybere texturu

		for (int i = 0; i < maxParticles; i++) {
			Particle particle = new Particle();
			initParticle(particle, i);
			particles[i] = particle;
		}
		
		active = true;
	}
	
	private void initParticle(Particle particle, int particleIndex) {
		particle.active = true;
		particle.life = 1;
		
		// náhodná rychlost mizení
		particle.fade = (float)(100 * Math.random())/1000.0f + 0.02f;
		
		// nastavení počáteční pozice
		particle.xPosition = position.getX();
		particle.yPosition = position.getY();
		particle.zPosition = position.getZ();

		// nastavení barev
		particle.red = colors[particleIndex * (12 / maxParticles)][0];
		particle.green = colors[particleIndex * (12 / maxParticles)][1];
		particle.blue = colors[particleIndex * (12 / maxParticles)][2];
		
		// náhodné nastavení rychlosti a směru pohybu
		particle.xDirection = (float)((50 * Math.random()) - 26.0f) * 10.0f;
		particle.yDirection = (float)((50 * Math.random()) - 25.0f) * 10.0f;
		particle.zDirection = (float)((50 * Math.random()) - 25.0f) * 10.0f;
	}

	public Point getPosition() {
		return position;
	}
	
}
