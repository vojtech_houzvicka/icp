package cz.tul.fm.icp.camera;

import static org.lwjgl.opengl.GL11.*;

public abstract class Camera {
	// souřadnice hlavní postavy
	protected float xpos, zpos, ypos;
	// úhel natočení hlavní postavy
	protected float updownrot, yrot;
	
	protected boolean active = true;

	public void curlView() {
		glRotatef(updownrot, 1.0f, 0.0f, 0.0f); // vertikální natočení
		glRotatef(-yrot, 0.0f, 1.0f, 0.0f); // otáčení pohledu
	}

	public void moveToPosition() {
		glTranslatef(-xpos, -ypos, -zpos);
	}

	public float getPosX() {
		return xpos;
	}

	public float getPosZ() {
		return zpos;
	}

	public float getPosY() {
		return ypos;
	}

	public void rotateY(float angle) {
		if (!active) {
			return;
		}
		
		yrot += angle;
		if (yrot > 360) {
			yrot -= 360;
		}
	}

	public void rotateUpDown(float angle) {
		if (!active) {
			return;
		}
		
		updownrot += angle;
		if (updownrot > 90) {
			updownrot = 90;
		} else if (updownrot < -90) {
			updownrot = -90;
		}
	}

	abstract public void move(Motion motion, double deltaTime);

	public void setPosition(Camera cam) {
		xpos = cam.xpos;
		zpos = cam.zpos;
		ypos = cam.ypos;
		yrot = cam.yrot;
		updownrot = cam.updownrot;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
