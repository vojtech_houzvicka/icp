/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tul.fm.icp.camera;

/**
 *
 * @author michal
 */
public class Motion {
	public float speed;
	public boolean 
		forward,
		back,
		left,
		right,
		jump;

	public Motion() {
		reset();
	}

	public void reset() {
		speed = 1;
		forward = back = left = right = jump = false;
	}
}
