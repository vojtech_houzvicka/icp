package cz.tul.fm.icp.camera;

import cz.tul.fm.icp.Field;
import cz.tul.fm.icp.objLoader.Model;
import cz.tul.fm.icp.objLoader.ObjLoader;
import cz.tul.fm.icp.objects.Renderable;
import cz.tul.fm.icp.objects.Sound;
import cz.tul.fm.icp.utils.GMath;
import cz.tul.fm.icp.utils.RandomService;
import java.io.File;
import java.io.IOException;
import static org.lwjgl.opengl.GL11.*;

public class Player extends Camera implements Renderable {
	// gravitační zrychlení
	private static final float GRAVITY = 40f;
	// rychlost za sekundu
	private static final float SPEED = 10f;
	// výška postavy
	private static final float HEIGHT = 3.25f;

	private static final String MODEL_PATH = "res/models/Venom/Venom.obj";

	private final RandomService randomSer;

	private Model model;
	private int objDL;

	// rychlost padání
	private float fallSpeed;

	private final Field floor;

	// stojí na podlaze?
	private boolean onFloor;
	// zaznamenání vektoru při skoku
	protected float xrel, zrel;

	private int health;
	private boolean immortality = false;
	private int ammo;

	private int hits;
	private int deaths;

	public Player(RandomService randomService, Field floor) {
		super();
		this.hits = 0;
		this.deaths = -1;
		this.randomSer = randomService;
		this.floor = floor;

		try {
			model = ObjLoader.loadTexturedModel(new File(MODEL_PATH));
			objDL = ObjLoader.createTexturedDisplayList(model);
		} catch (IOException e) {
			throw new RuntimeException("object loading error", e);
		}
	}

	public void respawn() {
		Sound.setHajzliPlayed(false);
		xpos = randomSer.get(0, floor.getLength() - 1);
		zpos = randomSer.get(0, floor.getWidth() - 1);
		ypos = floor.getHeight(xpos, zpos) + HEIGHT+3f;
		onFloor = false;
		updownrot = 0;
		xrel = 0; zrel = 0;

		fallSpeed = 0;
		health = 100;
		ammo = 0;
		deaths++;
	}

	public int getHealth() {
		return health;
	}

	public void hurt(int damage) {
		if (immortality) {
			return;
		}
		health -= damage;
		if (health <= 0) {
			respawn();
		}
	}

	public void switchImmortality() {
		immortality = !immortality;
	}

	public void takeAmmo(int amount) {
		ammo += amount;
	}

	public void useAmmo() {
		if (ammo > 0) {
			ammo--;
		}
	}

	public int getAmmo() {
		return ammo;
	}

	public void hit() {
		hits++;
	}

	public int getHits() {
		return hits;
	}

	public int getDeaths() {
		return deaths;
	}

	@Override
	public void move(Motion motion, double deltaTime) {
		if (!active) {
			return;
		}

		float move = (float) (deltaTime * SPEED);
		if (onFloor) {
			float coor1 = GMath.sin(yrot);
			float coor2 = GMath.cos(yrot);

			// rychlost
			coor1 *= motion.speed;
			coor2 *= motion.speed;

			if (motion.forward) {
				xrel -= coor1;
				zrel -= coor2;
			}
			if (motion.back) {
				xrel += coor1;
				zrel += coor2;
			}
			if (motion.left) {
				xrel -= coor2;
				zrel += coor1;
			}
			if (motion.right) {
				xrel += coor2;
				zrel -= coor1;
			}
			if (motion.jump) {
				onFloor = false;
				fallSpeed = -10f;
			}
		}
		xpos += xrel * move;
		zpos += zrel * move;

		float expectedY = floor.getHeight(xpos, zpos);
		if (expectedY == -Float.MAX_VALUE) {
			onFloor = false;
		}
		if (onFloor) {
			// vynulování vektoru pohybu
			xrel = 0;
			zrel = 0;

			ypos = expectedY + HEIGHT;
		} else {
			// příbytek padací rychlosti
			double gain = GRAVITY*deltaTime;
			ypos -= fallSpeed*deltaTime + gain*deltaTime/2;
			fallSpeed += gain;

			// úbytek rychlosti
			xrel /= 1+deltaTime;
			zrel /= 1+deltaTime;

			if (ypos < expectedY + HEIGHT + 0.5f) {
				onFloor = true;
				if (fallSpeed > 20) {
					hurt((int) (fallSpeed*0.2f));
				}
				fallSpeed = 0;
			}
		}
	}

	public void destroyFrom(float x, float z, float y, int radius) {
		float xr = x-xpos, zr = z-zpos, yr = y-ypos;
		double distance = Math.sqrt(xr*xr + zr*zr + yr*yr);
		if (distance < radius) {
			hurt(100);
		}
	}

	@Override
	public void render() {
		model.getTextures().get(0).bind();
		glPushMatrix();
		glTranslated(xpos, ypos - 3.2f, zpos);
		glRotated(yrot - 180, 0, 1, 0);
		glCallList(objDL);
		glPopMatrix();
	}

}
