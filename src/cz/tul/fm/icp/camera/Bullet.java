package cz.tul.fm.icp.camera;

import cz.tul.fm.icp.utils.GMath;

public class Bullet extends Camera {

	private static final float SPEED = 100f;
	public static final int POWER = 15;


	public Bullet() {
		xpos = 30; zpos = 30; ypos = 50;
		updownrot = 0; yrot = -100;
	}

	@Override
	public void move(Motion motion, double deltaTime) {
		if (!active) {
			return;
		}
		
		float speed3d = (float) (SPEED*motion.speed*deltaTime);
		ypos -= GMath.sin(updownrot) * speed3d;
		float speed2d = GMath.cos(updownrot) * speed3d;
		xpos -= GMath.sin(yrot) * speed2d;
		zpos -= GMath.cos(yrot) * speed2d;
	}

}
