package cz.tul.fm.icp;

import cz.tul.fm.icp.objects.Renderable;
import cz.tul.fm.icp.objects.State;
import cz.tul.fm.icp.utils.RandomService;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Field implements Renderable {

	private int[][] field;

	private static final float DIV = 20f;

	private final Map<State, Texture> textures;

	private final int dispListId;
	
	private State state = State.DAY;


	public Field(int width, int height, int hillCount, RandomService randomSer) {
		field = new int[width][height];

		for (int i = 0; i < hillCount; i++) {
			int x = randomSer.get(0, width);
			int y = randomSer.get(0, height);
			int damage = randomSer.get(-500, 500);
			int radius = randomSer.get(10, 110);
			deformAt(field, x, y, damage, radius);
		}
		
		this.textures = new HashMap<>();
		loadTextures();
		

		dispListId = glGenLists(1);
		createDispList();
	}

	private static void deformAt(int[][] field, int x, int y, int damage, int radius) {

		int width = field.length, height = field[0].length;
		for (int i = x - radius; i < x + radius; i++) {
			for (int j = y - radius; j < y + radius; j++) {
				if (i >= 0 && i < width && j >= 0 && j < height) {
					double w = i - x, h = j - y;
					double distance = Math.sqrt(w*w + h*h)/radius;
					if (distance < 1) {
						field[i][j] += (1 - distance*distance)*damage;
					}
				}
			}
		}
	}

	public void destroyFrom(float x, float z, float y, int radius) {
		int xpos = (int) x, ypos = (int) z;
		boolean changed = false;

		int width = field.length, height = field[0].length;
		for (int i = xpos - radius; i < xpos + radius; i++) {
			for (int j = ypos - radius; j < ypos + radius; j++) {
				if (i >= 0 && i < width && j >= 0 && j < height) {
					double w = i - xpos, h = j - ypos;
					double distance = Math.sqrt(w*w + h*h);
					if (distance <= radius) {
						int expected = (int) ((y - Math.sqrt(radius*radius - distance*distance)) * DIV);
						if (field[i][j] > expected) {
							changed = true;
							field[i][j] = expected;
						}
					}
				}
			}
		}
		if (changed) {
			createDispList();
		}
	}

	public float getHeight(float x, float y) {
		if (x < 0 || (int) x + 1 >= field.length || y < 0 || (int) y + 1 >= field[0].length) {
			return -Float.MAX_VALUE;
		}

		int x1, x2, y1, y2;
		x1 = (int) x;
		x2 = (int) x + 1;
		y1 = (int) y;
		y2 = (int) y + 1;

		float p00, p10, p01, p11;
		p00 = field[x1][y1];
		p01 = field[x1][y2];
		p10 = field[x2][y1];
		p11 = field[x2][y2];

		// bilineární interpolace
		float p = p00*(x2-x)*(y2-y) + p01*(x2-x)*(y-y1) + p10*(x-x1)*(y2-y) + p11*(x-x1)*(y-y1);

		return  p / DIV;
	}
	
	public boolean outOfField(float x, float z){
		return (x < 0 || (int) x + 1 >= field.length || z < 0 || (int) z + 1 >= field[0].length);
	}

	private void createDispList() {;
		float[] grass = new float[]{1/2f, 0/2f};
		float[] snow = new float[]{0/2f, 0/2f};
		float[] dirt = new float[]{1/2f, 1/2f};

		glNewList(dispListId, GL_COMPILE);

		glBegin(GL_TRIANGLES);
		for (int i = 0; i < field.length - 1; i++) {
			for (int j = 0; j < field[i].length - 1; j++) {

				float[] tex;
				tex = grass;
				if (field[i][j] > 230) {
					tex = snow;
				} else if (field[i][j] < -100) {
					tex = dirt;
				}

				glTexCoord2f(tex[0] + 0/2f, tex[1] + 0/2f);
				glVertex3f(i+1, field[i+1][j]/DIV, j);
				glTexCoord2f(tex[0] + 0/2f, tex[1] + 1/2f);
				glVertex3f(i, field[i][j]/DIV, j);
				glTexCoord2f(tex[0] + 1/2f, tex[1] + 1/2f);
				glVertex3f(i, field[i][j+1]/DIV, j+1);

				glTexCoord2f(tex[0] + 0/2f, tex[1] + 0/2f);
				glVertex3f(i+1, field[i+1][j]/DIV, j);
				glTexCoord2f(tex[0] + 1/2f, tex[1] + 1/2f);
				glVertex3f(i, field[i][j+1]/DIV, j+1);
				glTexCoord2f(tex[0] + 1/2f, tex[1] + 0/2f);
				glVertex3f(i+1, field[i+1][j+1]/DIV, j+1);
			}
		}
		glEnd();

		glEndList();
	}

	@Override
	public void render() {
		glColor3f(1, 1, 1);
		textures.get(state).bind();
		glPushMatrix();
		glCallList(dispListId);
		glPopMatrix();
	}

	public int getLength() {
		return field.length;
	}

	public int getWidth() {
		return field[0].length;
	}
	
	public void setState(State state) {
		this.state = state;
	}

	private void loadTextures() {
		try {
			textures.put(State.DAY, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/tex/terrain.png"), GL_LINEAR));
			textures.put(State.NIGHT, TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/tex/terrain_night.png"), GL_LINEAR));
		} catch (IOException e) {
			throw new RuntimeException("skybox loading error");
		}
	}
}
