package cz.tul.fm.icp.utils;

public class GMath {
	private static final double PI_OVER_180 = 0.0174532925;

	private GMath() {}

	public static float sin(double angle) {
		return (float) Math.sin(angle*PI_OVER_180);	
	}

	public static float cos(double angle) {
		return (float) Math.cos(angle*PI_OVER_180);	
	}

	public static double atan2(double x, double y) {
		return Math.atan2(x, y) / PI_OVER_180;
	}
	
}
