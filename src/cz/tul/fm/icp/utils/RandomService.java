package cz.tul.fm.icp.utils;

import java.util.Random;

public class RandomService {
	private final Random rand;

	public RandomService() {
		rand = new Random(System.currentTimeMillis());
	}

	public int get(int from, int to) {
		if (to < from) {
			throw new RuntimeException();
		}
		return rand.nextInt(to-from) + from;
	}
}
