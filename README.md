Zápočet na ICP
==============

Nastavení NetBeans
------------------

Viz [link](http://wiki.lwjgl.org/index.php?title=Setting_Up_LWJGL_with_NetBeans). Všechny potřebné soubory jsou v repu ve složce `/lib/lwjgl`.

K dodělání
----------
* (*osvětlení*)
* textury, skybox
* ac3d nebo wavefront OBJ modely
* výšková mapa, fyzika (hledání Z, kolize, deformace)
* částicové systémy
